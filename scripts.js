import { baseGameGoals, europeanExpansionGoals, oceaniaExpansionGoals } from './eorgArrays.js'

// Arrays //
let initalArray = baseGameGoals
const secondArray = europeanExpansionGoals
const thirdArray = oceaniaExpansionGoals
let history = []
let shuffledGoals = null // Inside of an event listener this object will be SET by the running of the following function: shuffleArray(initalArray).

// UI Elements //
// Buttons //
const reset = document.getElementById('resetButton')
const addSecondArray = document.getElementById('addEuropeanButton')
const addThirdArray = document.getElementById('addOceaniaButton')
const getGoals = document.getElementById('generatorButton')
// Divs with JS created content //
const drawnGoalsContainer = document.getElementById('drawnGoalsContainer')
const previousDraws = document.getElementById('previousDraws')

// Shuffle Array //
function goalShuffler (goals) {
  const playableGoals = [...goals]
  for (let i = 0; i < playableGoals.length - 1; i++ ) {
    const newPosition = Math.floor(Math.random() * (i + 1))
    const tempValue = playableGoals[i]
    playableGoals[i] = playableGoals[newPosition]
    playableGoals[newPosition] = tempValue
  }
  return playableGoals
}

// Add Arrays //
addSecondArray.addEventListener('click', () => {
  initalArray = [...initalArray, ...secondArray]
  addSecondArray.disabled = true
})

addThirdArray.addEventListener('click', () => {
  initalArray = [...initalArray, ...thirdArray]
  addThirdArray.disabled = true
})

// Reset App //
reset.addEventListener('click', () => {
  resetArrays()
  resetButtons()
  resetCurrentDraw()
  updateHistory()
})

function resetArrays () {
  initalArray = baseGameGoals // Resets initalArray.
  shuffledGoals = null // Resets the shuffledGoals array back to falsy existence.
  history = [] // Resets history to an empty array.
}

// Reenables the buttons.
function resetButtons () {
  addSecondArray.disabled = false
  addThirdArray.disabled = false
  getGoals.disabled = false
}

// Removes the previousDraw from its position as the first child HTML. This allows the next previous round draw to be the first child element while keeping the preceeding previous round on the DOM
function updateHistory () {
  while (previousDraws.firstChild) {
    previousDraws.firstChild.remove()
  }
}

// Clears the divs appended from the hand from drawnGoalsContainer within the Current Game area to allow the next draw to exclsively appear
function resetCurrentDraw () {
  drawnGoalsContainer.innerHTML = ''
}

// Reinitializes the generatedDraw without divs in effect removing the previous draw divs to allow only current currents divs to display.
// Javascript Created Content by shuffledGoals Array Manipulation - Current Draw and History //
getGoals.addEventListener('click', () => {
  shuffledGoals = shuffledGoals || goalShuffler(initalArray) // The shuffledGoals is now set as an array.
  const currentGameGoals = shuffledGoals.splice(0, 4) // The shuffledGoals array is now having its first 4 values removed and placed into a new array named currentGameGoals.
  // If history has values then loop over each value and then append the value to the previousDraws as divs.
  if (history.length > 0) {
    updateHistory()
    resetCurrentDraw()

    for (let i = 0; i < history.length; i++) {
      const previousGoal = document.createElement('img')
      previousGoal.className = 'goalCard'
      previousGoal.src = history[i]
      previousDraws.appendChild(previousGoal)
    }
  }

  // If currentGameGoals has 4 values then loop over each value and then append the value to the generatedDraw as divs.
  if (currentGameGoals.length === 4) {
    for (let i = 0; i < currentGameGoals.length; i++) {
      const img = document.createElement('img')
      img.className = 'goalCard'
      img.src = currentGameGoals[i]
      drawnGoalsContainer.appendChild(img)
    }
  }

  history.unshift(...currentGameGoals) // This adds each of the currentGameGoals elements generated to the beginning of the history array.

  // Button Logic

  // If shuffledGoals length is less than 4 then disable the getValues button.
  if (shuffledGoals.length < 4) {
    getGoals.disabled = true
  }
  // When getGoals is excuted disable the buttons to disallow Expansion Goals arrays to be added.
  addSecondArray.disabled = true
  addThirdArray.disabled = true

  return currentGameGoals
})
