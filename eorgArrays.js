const baseGameGoals = [
  './img/BaseGoals/BirdsInForrestHabitat.svg',
  './img/BaseGoals/BirdsInGrasslandHabitat.svg',
  './img/BaseGoals/BirdsInWetlandHabitat.svg',
  './img/BaseGoals/EggsInForrestHabitat.svg',
  './img/BaseGoals/EggsInGrasslandHabitat.svg',
  './img/BaseGoals/EggsInWetlandsHabitat.svg',
  './img/BaseGoals/EggsInBowlNests.svg',
  './img/BaseGoals/EggsInTreeNest.svg',
  './img/BaseGoals/EggsInGroundNest.svg',
  './img/BaseGoals/EggsInStickNest.svg',
  './img/BaseGoals/TreeNestBirdWithEgg.svg',
  './img/BaseGoals/GroundNestBirdWithEgg.svg',
  './img/BaseGoals/StickNestBirdWithEgg.svg',
  './img/BaseGoals/BowlNestBirdWithEgg.svg',
  './img/BaseGoals/SetsOfEggsInEachHabitat.svg',
  './img/BaseGoals/TotalCards.svg'
]

const europeanExpansionGoals = [
  './img/EuropeanGoals/BirdsInOneRow.svg',
  './img/EuropeanGoals/BirdsWithNoEgg.svg',
  './img/EuropeanGoals/BirdsWithTuckedCards.svg',
  './img/EuropeanGoals/BrownPowers.svg',
  './img/EuropeanGoals/CardsInHand.svg',
  './img/EuropeanGoals/FilledColumns.svg',
  './img/EuropeanGoals/FoodCostOfPlayedBirds.svg',
  './img/EuropeanGoals/FoodInPersonalSupply.svg',
  './img/EuropeanGoals/WhiteAndNoPowers.svg',
  './img/EuropeanGoals/WorthGreaterThan4.svg'
]

const oceaniaExpansionGoals = [
  './img/OceaniaGoals/BeakPointingLeft.svg',
  './img/OceaniaGoals/BeakPointingRight.svg',
  './img/OceaniaGoals/BerryAndGrainFoodCost.svg',
  './img/OceaniaGoals/CubesOnPlayABird.svg',
  './img/OceaniaGoals/MouseAndFishFoodCost.svg',
  './img/OceaniaGoals/NoGoal.svg',
  './img/OceaniaGoals/WormFoodCost.svg',
  './img/OceaniaGoals/WorthLessThanEqualTo3.svg'
]

export { baseGameGoals, europeanExpansionGoals, oceaniaExpansionGoals }
